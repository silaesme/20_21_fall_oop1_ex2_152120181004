#include <iostream>
int main(int argc, char const *argv[]) {
  int n;
  std::cin >> n;
  int *arr = new int [n];
  int *rarr = new int [n];
  for (int i = 0; i < n; i++) {
    std::cin >> arr[i];
  }
  int j = 0;
  for (int i = n-1; i >= 0; i--) {
    rarr[j]=arr[i];
    std::cout << rarr[j] << '\t';
    j++;
  }
  delete[] arr;
  delete[] rarr;
  
  return 0;
}
