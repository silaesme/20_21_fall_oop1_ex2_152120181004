#include<bits/stdc++.h>
using namespace std;
class Box
{
private:
    //l,b,h are integers representing the dimensions of the box
    int l,b,h;
public:
    Box();
    Box(int l, int b, int h);
    Box(const Box& B);
    int getLength(); // Return box's length
    int getBreadth (); // Return box's breadth
    int getHeight ();  //Return box's height
    long long CalculateVolume(); // Return the volume of the box
    friend bool operator<(Box& A, const Box& B)
    {
        if((A.l < B.l) || ((A.b < B.b) && (A.l == B.l)) || ((A.h < B.h) && (A.l == B.l) & (A.b == B.b)) )    return true;
    else    return false;
    }
    friend ostream& operator <<(ostream& out,const Box&B)
    {
        out << B.l << " " << B.b << " " << B.h;
        return out;
    }
};

Box::Box(){l = 0, b = 0, h = 0;}
Box::Box(int l, int b, int h){this->l=l; this->b=b; this->h=h;}
Box::Box(const Box& B){l=B.l; b=B.b; h=B.h;}
int Box::getLength(){ return l;}
int Box::getHeight(){ return h; }
int Box::getBreadth(){ return b; }
long long Box::CalculateVolume(){ return (long long) l*h*b; }
void check2()
{
	int n;
	cin>>n;
	Box temp;
	for(int i=0;i<n;i++)
	{
		int type;
		cin >> type;
		if(type == 1)
		{
			cout<<temp<<endl;
		}
		if(type == 2)
		{
			int l,b,h;
			cin>>l>>b>>h;
			Box NewBox(l,b,h);
			temp=NewBox;
			cout<<temp<<endl;
		}
		if(type==3)
		{
			int l,b,h;
			cin>>l>>b>>h;
			Box NewBox(l,b,h);
			if(NewBox<temp)
			{
				cout<<"Lesser\n";
			}
			else
			{
				cout<<"Greater\n";
			}
		}
		if(type==4)
		{
			cout<<temp.CalculateVolume()<<endl;
		}
		if(type==5)
		{
			Box NewBox(temp);
			cout<<NewBox<<endl;
		}

	}
}

int main()
{
	check2();
}