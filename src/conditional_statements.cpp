#include <iostream>
int main(int argc, char const *argv[]) {
  int n;
  std::cin >> n;
  if (n <= 9 && n >= 1) {
    if (n == 1) {
      std::cout << "one" << '\n';
    }
    else if (n == 2) {
      std::cout << "two" << '\n';
    }
    else if (n == 3) {
      std::cout << "three" << '\n';
    }
    else if (n == 4) {
      std::cout << "four" << '\n';
    }
    else if (n == 5) {
      std::cout << "five" << '\n';
    }
    else if (n == 6) {
      std::cout << "six" << '\n';
    }
    else if (n == 7) {
      std::cout << "seven" << '\n';
    }
    else if (n == 8) {
      std::cout << "eight" << '\n';
    }
    else if (n == 9) {
    std::cout << "nine" << '\n';
    }
  }
  else if (n == 0)  std::cout<<"Enter valid number.";
  else  std::cout << "Greater than 9" << '\n';
  return 0;
}
