#include <iostream>
int greadestOfFour(int a, int b, int c, int d);
int main(int argc, char const *argv[]) {
  int w,x,y,z;
  int gr;
  std::cin >> w >> x >> y >> z;
  gr = greadestOfFour(w,x,y,z);
  std::cout << gr << '\n';
  return 0;
}
int greadestOfFour(int a, int b, int c, int d)
{
  int arr[] = {a,b,c,d};
  int greatest = a;
  for (int i = 1; i < 4; i++) {
    if (arr[i] > greatest) {
      greatest = arr[i];
    }
  }
  return greatest;
}
