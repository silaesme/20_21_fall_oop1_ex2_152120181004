#include <iostream>
#include <sstream>
#include <string>
using namespace std;

class Student{
private:
  int age,standard;
  string first_name,last_name;
public:
  void set_age(int a){
    age = a;
  }
  void set_standard(int s){
    standard = s;
  }
  void set_first_name(string first){
    first_name=first;
  }
  void set_last_name(string last){
    last_name=last;
  }
  int get_age(){
    return age;
  }
  int get_standard(){
    return standard;
  }
  string get_first_name(){
    return first_name;
  }
  string get_last_name(){
    return last_name;
  }
  string to_string(){
    stringstream ss1,ss2;
    string s,s1,s2,s3,s4,s5;

    ss1 << age;
    ss1 >> s4;
    ss2 << standard;
    ss2 >> s5;

    s1 = first_name; s2 = last_name; s3 = ",";
    s = s4 + s3 + s1 + s3 + s2 + s3 + s5;
    return s;
  }
};

int main() {
    int age, standard;
    string first_name, last_name;

    cin >> age >> first_name >> last_name >> standard;

    Student st;
    st.set_age(age);
    st.set_standard(standard);
    st.set_first_name(first_name);
    st.set_last_name(last_name);

    cout << st.get_age() << "\n";
    cout << st.get_last_name() << ", " << st.get_first_name() << "\n";
    cout << st.get_standard() << "\n";
    cout << "\n";
    cout << st.to_string();

    return 0;
}
